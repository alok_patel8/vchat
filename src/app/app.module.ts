import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxAgoraModule } from 'ngx-agora';
import { AngularAgoraRtcModule, AgoraConfig } from 'angular-agora-rtc'; // Add

import { VideoCallComponent } from './video-call/video-call.component';
import { TestComponent } from './test/test.component';
import { AngPeerjsComponent } from './ang-peerjs/ang-peerjs.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CallinfoDialogComponent } from './callinfo-dialog/callinfo-dialog.component';

// Add
const agoraConfig: AgoraConfig = {
  AppID: '8f87b30ed8414d60b68983480c80f401',
};

@NgModule({
  declarations: [
    AppComponent,
    VideoCallComponent,
    TestComponent,
    AngPeerjsComponent,
    CallinfoDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // NgxAgoraModule.forRoot(agoraConfig),
    AngularAgoraRtcModule.forRoot(agoraConfig),
    BrowserAnimationsModule, // Add
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ClipboardModule,
    MatSnackBarModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
