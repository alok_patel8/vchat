import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import {
  CallinfoDialogComponent,
  DialogData,
} from '../callinfo-dialog/callinfo-dialog.component';
import { CallServiceService } from '../services/call-service.service';
import { uuid } from 'uuidv4';

@Component({
  selector: 'app-ang-peerjs',
  templateUrl: './ang-peerjs.component.html',
  styleUrls: ['./ang-peerjs.component.css'],
})
export class AngPeerjsComponent implements OnInit, OnDestroy {
  public isCallStarted$: Observable<boolean>;
  private peerId: string;

  @ViewChild('localVideo') localVideo: ElementRef<HTMLVideoElement>;
  @ViewChild('remoteVideo') remoteVideo: ElementRef<HTMLVideoElement>;

  constructor(
    public dialog: MatDialog,
    private callService: CallServiceService
  ) {
    this.isCallStarted$ = this.callService.isCallStarted$;
    this.peerId = this.callService.initPeer();
  }
  ngOnInit(): void {
    // this.callService.localStream$
    //   .pipe(filter((res) => !!res))
    //   .subscribe(
    //     (stream) => (this.localVideo.nativeElement.srcObject = stream)
    //   );
    // this.callService.remoteStream$
    //   .pipe(filter((res) => !!res))
    //   .subscribe(
    //     (stream) => (this.remoteVideo.nativeElement.srcObject = stream)
    //   );
  }

  ngOnDestroy(): void {
    this.callService.destroyPeer();
  }

  public showModal(joinCall: boolean): void {
    let dialogData: DialogData = joinCall
      ? { peerId: null, joinCall: true }
      : { peerId: this.peerId, joinCall: false };
    const dialogRef = this.dialog.open(CallinfoDialogComponent, {
      width: '250px',
      data: dialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(
        switchMap((peerId) =>
          joinCall
            ? of(this.callService.establishMediaCall(peerId))
            : of(this.callService.enableCallAnswer())
        )
      )
      .subscribe((_) => {});
  }

  public endCall() {
    this.callService.closeMediaCall();
  }
}
function switchMap(
  arg0: (peerId: any) => any
): import('rxjs').OperatorFunction<any, unknown> {
  throw new Error('Function not implemented.');
}
