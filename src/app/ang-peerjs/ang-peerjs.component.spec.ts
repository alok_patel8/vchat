import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AngPeerjsComponent } from './ang-peerjs.component';

describe('AngPeerjsComponent', () => {
  let component: AngPeerjsComponent;
  let fixture: ComponentFixture<AngPeerjsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AngPeerjsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AngPeerjsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
