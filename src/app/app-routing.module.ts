import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngPeerjsComponent } from './ang-peerjs/ang-peerjs.component';
import { TestComponent } from './test/test.component';
import { VideoCallComponent } from './video-call/video-call.component';

const routes: Routes = [
        { path: '', redirectTo: 'videocall', pathMatch: 'full' },

  {
    path: 'videocall',
    component: VideoCallComponent,
  },
  {
    path: 'peer',
    component: AngPeerjsComponent,
  },
  {
    path: 'test',
    component: TestComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
